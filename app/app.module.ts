require( "nativescript-localstorage" );
require( "nativescript-orientation" );

import { NgModule, NO_ERRORS_SCHEMA, Injector, ErrorHandler } from "@angular/core";
import { EffectsModule } from '@ngrx/effects';
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";

import { routes } from "./app.routing";
import { AppComponent } from "./app.component";

import { AccountModule } from "./account";
import { ListModule } from "./list";
import { GroupModule } from "./group/";
import { reducers } from "./app.reducers";
import { environment } from "./environments/environment";

import { Api } from "./passit-frontend/ngsdk/api";
import { NgPassitSDK } from "./passit-frontend/ngsdk/sdk";
import { LoggedInGuard, AlreadyLoggedInGuard } from "./passit-frontend/guards";
import { SecretService } from "./passit-frontend/secrets/secret.service";
import { GroupService } from "./passit-frontend/group/group.service";
import { metaReducers } from "./passit-frontend/app.reducers";
import { AppDataService } from "./passit-frontend/shared/app-data/app-data.service";
import { GetConfService } from "./passit-frontend/get-conf/";
import { GetConfEffects } from "./passit-frontend/get-conf/conf.effects";
import { SecretEffects } from "./passit-frontend/secrets/secret.effects";

import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

import { MobileMenuModule } from "./mobile-menu";
import { GeneratorService } from "~/passit-frontend/secrets";
import { SharedModule } from "~/shared/shared.module";
import { StoreRouterConnectingModule } from "@ngrx/router-store";
import { SentryErrorHandler } from "~/error-handler";

@NgModule({
	bootstrap: [
		AppComponent
	],
	imports: [
		NativeScriptModule,
		AccountModule,
		ListModule,
		GroupModule,
		MobileMenuModule,
		SharedModule,
		EffectsModule.forRoot([SecretEffects, GetConfEffects]),
		StoreModule.forRoot(reducers, {metaReducers}),
		NativeScriptRouterModule.forRoot(routes),
		NativeScriptRouterModule,
		StoreRouterConnectingModule.forRoot({stateKey: 'router',}),
		!environment.production ? StoreDevtoolsModule.instrument({maxAge: 25}) : [],
		NativeScriptHttpClientModule,
		NativeScriptFormsModule,
	],
	declarations: [
		AppComponent,
	],
	providers: [
		Api,
		AppDataService,
		LoggedInGuard,
		AlreadyLoggedInGuard,
		GeneratorService,
		GetConfService,
		NgPassitSDK,
		SecretService,
		GroupService,
        { provide: ErrorHandler, useClass: SentryErrorHandler }
	],
	schemas: [NO_ERRORS_SCHEMA]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule {
	static injector: Injector;
    constructor(injector: Injector) {
        AppModule.injector = injector;
    }
}
