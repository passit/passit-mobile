import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule} from "nativescript-angular/forms"
import { NativeScriptHttpModule } from "nativescript-angular/http";

import { UserService } from "../passit-frontend/account/user";
import { LoginContainer } from "../passit-frontend/account/login/login.container";
import { LoginComponent } from "./login";
import { LoginEffects } from "../passit-frontend/account/account.effects";
import { LoginEffects as AppLoginEffects } from "./login/login.effects";
import { LoginFormEffects } from "../passit-frontend/account/login/login.effects";
import { RegisterContainer } from "../passit-frontend/account/register/register.container";
import { RegisterComponent } from "./register";
import { RegisterEffects } from "../passit-frontend/account/register/register.effects";
import { reducers } from "../passit-frontend/account/account.reducer";
import { NgrxFormsModule } from "ngrx-forms";
import { DirectivesModule } from "~/directives";
import { ChangePasswordContainer } from "../passit-frontend/account/change-password/change-password.container";
import { ChangePasswordComponent } from "./change-password/change-password.component";
import { ConfirmEmailContainer } from "../passit-frontend/account/confirm-email/confirm-email.container";
import { ConfirmEmailComponent } from "./confirm-email/confirm-email.component";
import { ErrorReportingContainer } from "../passit-frontend/account/error-reporting/error-reporting.container";
import { ErrorReportingEffects } from "../passit-frontend/account/error-reporting/error-reporting.effects";
import { AppConfirmEmailEffects } from "./confirm-email/confirm-email.effects";
import { MobileMenuModule } from "~/mobile-menu";
import { MoonMail } from "~/passit-frontend/account/moonmail/moonmail.service";
import { SharedModule } from "~/shared/shared.module";
import { ConfirmEmailGuard } from "~/passit-frontend/account/confirm-email/confirm-email.guard";
import { ConfirmEmailEffects } from "~/passit-frontend/account/confirm-email/confirm-email.effects";
import { ErrorReportingComponent } from "~/account/error-reporting/error-reporting.component";


export const COMPONENTS = [
  LoginContainer,
  LoginComponent,
  RegisterContainer,
  RegisterComponent,
  ChangePasswordComponent,
  ChangePasswordContainer,
  ErrorReportingContainer,
  ErrorReportingComponent,
  ConfirmEmailContainer,
  ConfirmEmailComponent
];

export const SERVICES = [
  UserService,
  MoonMail,
  ConfirmEmailGuard
];

@NgModule({
  imports: [
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptHttpModule,
    NgrxFormsModule,
		SharedModule,
    DirectivesModule,
    MobileMenuModule,
    StoreModule.forFeature('account', reducers),
    EffectsModule.forFeature([
      LoginEffects,
      LoginFormEffects,
      AppLoginEffects,
      RegisterEffects,
      ConfirmEmailEffects,
      AppConfirmEmailEffects,
      ErrorReportingEffects
    ]),
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [SERVICES],
	schemas: [NO_ERRORS_SCHEMA]
})
export class AccountModule { }
