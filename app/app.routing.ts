import { Routes } from "@angular/router";

import { LoginContainer } from "./passit-frontend/account/login/login.container";
import { SecretListContainer } from "./passit-frontend/list/list.container";
import { GroupContainer } from "./passit-frontend/group/group.container";
import { SecretDetailComponent } from "./list"
import { LoggedInGuard, AlreadyLoggedInGuard } from "./passit-frontend/guards";
import { ChangePasswordContainer } from "~/passit-frontend/account/change-password/change-password.container";
import { SecretNewComponent } from "~/list/new.component";
import { RegisterContainer } from "~/passit-frontend/account/register/register.container";
import { ConfirmEmailContainer } from "~/passit-frontend/account/confirm-email/confirm-email.container";
import { ConfirmEmailGuard } from "~/passit-frontend/account/confirm-email/confirm-email.guard";
import { ErrorReportingContainer } from "~/passit-frontend/account/error-reporting/error-reporting.container";

export const routes: Routes = [
    { path: "", redirectTo: "/list", pathMatch: "full" },
    {
        path: "login",
        component: LoginContainer,
        data: {
            title: "Login"
        }
    },
    {
        path: "register",
        component: RegisterContainer,
        canActivate: [AlreadyLoggedInGuard],
        data: {
            title: "Register",
        }
    },
    {
        path: "confirm-email",
        component: ConfirmEmailContainer,
        canActivate: [ConfirmEmailGuard],
        data: {
            title: "Confirm Email"
        }
    },
    {
        path: "list",
        component: SecretListContainer,
        canActivate: [LoggedInGuard],
    },
    {
        path: "list/new",
        component: SecretNewComponent,
        canActivate: [LoggedInGuard],
    },
    {
        path: "list/:id",
        component: SecretDetailComponent,
        canActivate: [LoggedInGuard],
    },
    {
        path: "groups",
        component: GroupContainer,
        canActivate: [LoggedInGuard],
    },
    {
        path: "account/change-password",
        component: ChangePasswordContainer,
        canActivate: [LoggedInGuard],
    },
    {
        path: "account/error-reporting",
        component: ErrorReportingContainer,
        canActivate: [LoggedInGuard],
    },
];
