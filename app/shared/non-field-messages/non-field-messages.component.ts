import { ChangeDetectionStrategy, Component, Input } from "@angular/core";

@Component({
  selector: "app-non-field-messages",
  template: `
    <StackLayout orientation="vertical" *ngIf="messages && messages.length > 0">
      <StackLayout *ngFor="let message of messages">
        <Label [text]="message"></Label>
      </StackLayout>
    </StackLayout>
  `,
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NonFieldMessagesComponent {
  /**
   * Receives multiple messages (or a single message as an array of one) and
   * displays them
   */
  @Input() messages: string[];

  constructor() {}
}

